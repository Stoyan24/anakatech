import {Component, Input, OnInit} from '@angular/core';
import {animate, style, transition, trigger} from "@angular/animations";

@Component({
  selector: 'app-rates-container',
  templateUrl: './rates-container.component.html',
  styleUrls: ['./rates-container.component.scss'],
  animations: [
    trigger('countAnimation', [
      transition(':increment', [
        style({opacity: 0}),
        animate('1200ms ease', style({opacity: 1})),
      ]),
      transition(':decrement', [
        style({opacity: 0}),
        animate('1200ms ease', style({opacity: 1})),
      ]),
    ]),
  ]
})
export class RatesContainerComponent implements OnInit {

  @Input() rate: any;
  public oldRate: number;
  public ratesColorClass = true;

  private UpDownState = true;
  private UpDownValue = 0.0001;
  public interval$: any;
  public timer$: any;
  public changeInterval$:any;

  constructor() {

  }

  ngOnInit(): void {
    //Interval every minute';
    this.changeInterval$ = setInterval(() => {
      if (this.UpDownState){
        this.UpDownValue = -0.0001;
        this.UpDownState = false;
      }else {
        this.UpDownValue = 0.0001;
        this.UpDownState = true;
      }
    }, 60000);

    // Clear intervals after 5min
    this.timer$ = setTimeout(() => {
      clearInterval(this.interval$);
      clearInterval(this.changeInterval$);
    }, 60000*5);

    // 5 seconds tick
    this.interval$ = setInterval(() => {
      this.oldRate = this.rate.value;
      this.rate.value += this.UpDownValue;
      this.ratesColorClass = this.rate.value >= this.oldRate;
    }, 5000);
  }




}
