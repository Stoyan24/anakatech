import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {

  constructor(private readonly http: HttpClient) {}

  public getCurrencies(): Observable<any> {
    return this.http.get("/assets/currencies.json");
  }
}
