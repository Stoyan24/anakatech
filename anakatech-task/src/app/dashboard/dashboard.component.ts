import {Component, OnInit} from '@angular/core';
import {CurrencyService} from "../services/currency.service";
import {CurrencyInterface} from "../models/currency.interface";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {


  public currencies: CurrencyInterface;
  constructor(private readonly currencyService: CurrencyService) {
  }

  ngOnInit(): void {
    this.currencyService.getCurrencies().subscribe(
      el => this.currencies = el,
      err => console.log(err),
    );
  }

}
