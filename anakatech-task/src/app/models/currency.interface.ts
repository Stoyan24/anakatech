export interface CurrencyInterface {
  success: boolean;
  timestamp: number;
  base: string;
  date: Date,
  rates: {
    USD: number;
    AUD: number;
    CAD: number;
    BGN: number;
  }
}
